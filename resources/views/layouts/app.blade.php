<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Cashman</title>


    <!-- Styles -->
    @include('layouts.inc-stylesheet')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        @include('layouts.inc-header')
        
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    @include('layouts.inc-scripts')
</body>
</html>
