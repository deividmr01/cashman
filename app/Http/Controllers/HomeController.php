<?php

namespace App\Http\Controllers;


use Validator;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use app\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('dashboard');
    }

    public function indexMonedas(){
        return view('index_monedas');
    }

    public function indexCuentas(){
        return view('index_cuentas');
    }

    public function getMonedas()
    {
        $monedas=DB::Table('monedas as m')
        ->select('m.nombre as text','m.codigo as value', 'm.pais as country')
        ->get();

        return response()->json([
            "success"=>true,
            "monedas"=>$monedas
        ]);
    }

    public function saveMoneda(Request $request)
    {
        $userid = Auth::user()->id;

        $usuario = User::where('id', $userid)
        ->first();

        $usuario->moneda_id_moneda = $request->moneda;

        $usuario->update();

        return response()->json([
            "success"=>true
        ]);
    }
}
